import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True    
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://<usuario>:<senha>@127.0.0.1:3306/<nome_banco>'
    SECRET_KEY = 'bureaubureaubureaubureau'
    USERNAME = ''
    PASSWORD = ''
    HOST = '127.0.0.1'
    MYSQL_PORT = '3306'
    DATABASE = ''


class ProductionConfig(Config):
    DEVELOPMENT = True
    DEBUG = False


class StagingConfig(Config): 
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
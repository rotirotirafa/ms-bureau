class UserSchema:

    def __init__(self):
        pass


    def fake_events(self, payload):
        return {
            'ultima_consulta': { 
                'operadora': 'x', 
                'data': 'datetime'
            },
            'movimentacao_financeira': self.fake_movement(payload),
            'usuario': self.fake_user(payload)
        }

    def fake_movement(self, payload):
        return [
            {
                'tipo': 'debito',
                'valor': 'R$ 78,00'
            },
            {
                'tipo': 'debito',
                'valor': 'R$ 49,00'
            },
             {
                'tipo': 'credito',
                'valor': 'R$ 500,00'
            },
        ]
    
    def fake_user(self, payload):
        return [
            {
                'usuario': payload
            }
        ]


from flask import Flask
from flask_restful import Api
from .resources import EventsResource
from config import Config
from flask_jwt_extended import JWTManager, jwt_required
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

jwt = JWTManager(app)

app.config["JWT_SECRET_KEY"] = Config.SECRET_KEY

api = Api(app)

api.add_resource(EventsResource, '/v1/events')

def create_app():
    import sys, os
    sys.path.append(os.path.dirname(os.path.abspath(__file__)))
    app.run(host='0.0.0.0', port=5002,debug=True)

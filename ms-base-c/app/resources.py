from flask import request, jsonify
from flask_restful import Resource
from flask_jwt_extended import JWTManager, jwt_required
from .services import EventsService


class EventsResource(Resource):
    http_methods_allowed = ['GET']


    def __init__(self):
        self.event_service = EventsService()
    
    def get(self):
        try:
            payload = request.get_json()
            response = self.event_service.get_user_events_with_cpf(payload)
            return response, 200
        except Exception as e:
            import logging
            logging.warn(e)
            return jsonify(e), 500
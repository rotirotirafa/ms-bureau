from .schema import UserSchema

class EventsService:

    def __init__(self):
        self.user_schema = UserSchema()

    def get_user_events_with_cpf(self, payload):
        return self.user_schema.fake_events(payload)

# Instale com pipenv
- pipenv install
- pipenv shell 

### Inicia o servidor
- python run.py

Payload de exemplo:
GET URL -> http://127.0.0.1:5002/v1/events
```
{
  "idade": 24,
  "endereco": "Rua Olavo Bilac, 454"
}

```
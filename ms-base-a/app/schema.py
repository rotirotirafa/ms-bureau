class UserSchema:

    def __init__(self):
        pass


    def fake_user(self, cpf):
        return {
            'nome': "rafael salinas rotiroti",
            'cpf': cpf,
            'endereco': "Rua Olavo Bilac, 454",
            'dividas ': self.fake_dividas(cpf)
        }

    def fake_dividas(self, cpf):
        return [
            {
                'banco': 'BMG',
                'valor': 'R$1.000.000,00'
            },
            {
                'banco': 'Banrisul',
                'valor': 'R$ 500.000,00'
            }
        ]
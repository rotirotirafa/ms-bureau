from .schema import UserSchema

class UserService:

    def __init__(self):
        self.user_schema = UserSchema()

    def get_user_information_with_cpf(self, payload):
        cpf = payload['cpf']
        return self.user_schema.fake_user(cpf)

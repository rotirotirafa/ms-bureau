# Instale com pipenv
- pipenv install
- pipenv shell 

### Inicia o servidor
- python run.py

Payload de exemplo:

GET URL -> http://127.0.0.1:5000/v1/users

```
{
	"cpf": "41130043827"	
}
```

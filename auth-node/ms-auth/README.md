# MS AUTH

Criado utilizando AdonisJS

### Instale o Adonis
``` npm i -g @adonisjs/cli ```

### instale as dependencias
``` yarn ou npm install ```


## Atualizar informações no .ENV
    - nome do banco
    - usuario
    - senha

# Passo 1 
    adonis migration:run

# Passo 2
    adonis serve

# Crie um usuario fazendo uma request com POST para rota /users
    {
        "username" : "exemplo",
        "password" : "123456",
        "status": "verificado",
        "active": true,
        "cnpj": "123456789111110",
        "email": "exemplo@gmail.com"
    }
# Autentique com POST na rota /auth
    {
        "email": "exemplo@gmail.com"
        "password" : "123456",
    }

'use strict'

const Route = use('Route')

Route.post('/users', 'UserController.create')
Route.post('/auth', 'AuthController.create')
# Instale com pipenv
- pipenv install
- pipenv shell 

### Inicia o servidor
- python run.py

Payload de exemplo:

GET URL -> http://127.0.0.1:5001/v1/score

```
{
  "nome": "rafael salinas rotiroti",
  "cpf": "41130043827",
  "endereco": "Rua Olavo Bilac, 454",
  "dividas ": [
    {
      "banco": "BMG",
      "valor": "R$1.000.000,00"
    },
    {
      "banco": "Banrisul",
      "valor": "R$ 500.000,00"
    }
  ]
}
```
class UserSchema:

    def __init__(self):
        pass


    def fake_user(self, payload):
        return {
            'idade': 24,
            'lista_de_bens': self.fake_bens,
            'endereco': payload['endereco'],
            'lista_de_bens' : self.fake_bens(payload),
            'fonte_de_renda ': self.fake_fonte_renda(payload)
        }

    def fake_bens(self, payload):
        return [
            {
                'imovel': 'Casa Terrea',
                'valor': 'R$2.400.000,00'
            },
            {
                'imovel': 'Apartamento',
                'valor': 'R$ 500.000,00'
            },
            {
                'veiculo': 'Onix 1.4 LT',
                'valor': 'R$ 34.000,00',
                'ano': 2013
            }
        ]
    
    def fake_fonte_renda(self, payload):
        return [
            {
                'CLT': 'Profissional Assalariado'
            }
        ]


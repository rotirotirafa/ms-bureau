from flask import request, jsonify
from flask_restful import Resource
from flask_jwt_extended import JWTManager, jwt_required
from .services import UserService


class UsersResource(Resource):
    http_methods_allowed = ['GET']


    def __init__(self):
        self.user_service = UserService()
    
    def get(self):
        try:
            payload = request.get_json()
            response = self.user_service.get_user_information_with_cpf(payload)
            return response, 200
        except Exception as e:
            import logging
            logging.warn(e)
            return jsonify(e), 500